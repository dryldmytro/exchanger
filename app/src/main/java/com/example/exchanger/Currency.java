package com.example.exchanger;

public class Currency {
    private String name;
    private double buy;
    private double sell;
    private double quantity;

    public Currency(String name, double buy, double sell) {
        this.name = name;
        this.buy = buy;
        this.sell = sell;
        this.quantity = 1;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBuy() {
        return buy*quantity;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSell() {
        return sell*quantity;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "name='" + name + '\'' +
                ", buy=" + buy +
                ", sell=" + sell +
                ", quantity=" + quantity +
                '}';
    }
}
