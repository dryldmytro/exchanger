package com.example.exchanger;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CurrencyAdapter extends ArrayAdapter<Currency> {
    private LayoutInflater inflater;
    private int layout;
    private ArrayList<Currency> currencyList;

    CurrencyAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Currency> objects) {
        super(context, resource, objects);
        this.currencyList = objects;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(this.layout, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Currency currency = currencyList.get(position);
        viewHolder.name.setText(currency.getName());
        viewHolder.buy.setText(String.valueOf(currency.getBuy()));
        viewHolder.sell.setText(String.valueOf(currency.getSell()));
        viewHolder.quantity.setText(String.valueOf(currency.getQuantity()));
        viewHolder.quantity.setTag(position);
        viewHolder.buy.setTag(position);
        viewHolder.sell.setTag(position);
        viewHolder.quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (viewHolder.quantity.getTag()!=null){
                    if (viewHolder.quantity.hasFocus()){
                        if (Double.parseDouble(s.toString()) > 0) {
                            double buyDouble = currency.getBuy()
                                    * Double.parseDouble(s.toString());
                            double sellDouble = currency.getSell() *
                                    Double.parseDouble(s.toString());
                            viewHolder.buy.setText(String.valueOf(buyDouble));
                            viewHolder.sell.setText(String.valueOf(sellDouble));
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        viewHolder.buy.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (viewHolder.buy.getTag()!=null){
                    if (viewHolder.buy.hasFocus()){
                        if (Double.parseDouble(s.toString())>0){
                            currency.setBuy(Double.parseDouble(s.toString()));
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        viewHolder.sell.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (viewHolder.sell.getTag()!=null){
                    if (viewHolder.sell.hasFocus()){
                        if (Double.parseDouble(s.toString())>0){
                            currency.setSell(Double.parseDouble(s.toString()));
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return convertView;
    }

    private class ViewHolder {
        final TextView name;
        final EditText buy, sell, quantity;

        public ViewHolder(View view) {
            name = view.findViewById(R.id.currency);
            buy = view.findViewById(R.id.buy);
            sell = view.findViewById(R.id.sell);
            quantity = view.findViewById(R.id.quantity);
        }
    }
}
