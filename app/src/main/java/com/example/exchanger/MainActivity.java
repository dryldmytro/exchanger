package com.example.exchanger;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public ArrayList<Currency> list = new ArrayList<>();
    private CurrencyAdapter currencyAdapter;
    private ListView currencyList;
    private static final int PICK_IMAGE = 100;
    private Uri imageURI;
    private ImageView imageView;
    private TextView textView;
    private RequestQueue queue;
    private View header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        queue = Volley.newRequestQueue(this);
        jsonParse();
        currencyList = findViewById(R.id.currencyList);
        header = createHeader();
        currencyList.addHeaderView(header);
        currencyAdapter = new CurrencyAdapter(this, R.layout.list_item, list);
        currencyList.setAdapter(currencyAdapter);
        imageView = findViewById(R.id.image);
        textView = findViewById(R.id.multiline);
    }

    private View createHeader() {
        View v = getLayoutInflater().inflate(R.layout.header,null);
        return v;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageURI = data.getData();
            imageView.setImageURI(imageURI);
            getTextFromImage();
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void getTextFromImage() {
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
        if (!textRecognizer.isOperational()) {
            Toast.makeText(getApplicationContext(), "Could not get text", Toast.LENGTH_SHORT).show();
        } else {
            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            SparseArray<TextBlock> items = textRecognizer.detect(frame);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < items.size(); i++) {
                TextBlock tb = items.valueAt(i);
                sb.append(tb.getValue() + "\n");
            }
            textView.setText(sb.toString());
        }
    }

    public void getImage(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE);
    }

    public void getFromText(View view) {
        String s = textView.getText().toString();
        String[] courses = s.split("\n");
        parseString(courses);
    }

    public void parseString(String[] courses) {
        try {
            currencyAdapter.clear();
            for (String s : courses) {
                String string = s.replaceAll(" ", "|")
                        .replaceAll(",", ".");
                ArrayList<Character> letters = new ArrayList<>();
                ArrayList<Character> symbols = new ArrayList<>();
                for (Character ch : string.toCharArray()) {
                    if (Character.isLetter(ch)) {
                        letters.add(ch);
                    } else symbols.add(ch);
                }
                String currencyName = letters.toString().replaceAll("[, \\[\\]]", "");
                String[] currenciesValue = symbols.toString().replaceAll("[, \\[\\]]", "")
                        .split("(?=\\|\\d)");
                ArrayList<Double> doubles = new ArrayList<>();
                for (String currencyValue : currenciesValue) {
                    doubles.add(Double.parseDouble(currencyValue.replaceAll("\\|", "")));
                }
                currencyAdapter.add(new Currency(currencyName, doubles.get(0), doubles.get(1)));
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Can't parse data", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void jsonParse() {
        String url = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    currencyAdapter.clear();
                    for (int i = 0; i < response.length(); i++) {

                        JSONObject jsonObject = response.getJSONObject(i);
                        String currencyName = jsonObject.getString("ccy");
                        Double buy = jsonObject.getDouble("buy");
                        Double sell = jsonObject.getDouble("sale");
                        currencyAdapter.add(new Currency(currencyName, buy, sell));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Can't get data from PrivatBank", Toast.LENGTH_LONG);
                error.printStackTrace();
            }
        });
        queue.add(request);
    }

    public void getFromPrivat(View view) {
        jsonParse();
    }
}
